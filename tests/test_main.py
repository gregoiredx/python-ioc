from dataclasses import dataclass

from inject import inject


@inject
@dataclass
class Config:
    name: str = "Ben"


@inject
def hello(config: Config) -> str:
    return f"Hello {config.name}"


@inject
class ClientA:
    def __init__(self, config: Config):
        self.config = config

    def hello(self) -> str:
        return f"Hello {self.config.name}"


@inject
def second_level(client_a: ClientA):
    return f"second level {client_a.hello()}"


def test_func():
    assert hello(Config(name="Joe")) == "Hello Joe"
    assert hello() == "Hello Ben"


def test_class():
    assert ClientA(Config(name="Joe")).hello() == "Hello Joe"
    assert ClientA().hello() == "Hello Ben"


def test_second_level():
    assert second_level(ClientA(Config(name="Joe"))) == "second level Hello Joe"
    assert second_level() == "second level Hello Ben"
