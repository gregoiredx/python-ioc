import re
from inspect import signature


def call_in_context(func, context, args, kwargs):
    func_params = signature(func).parameters
    if len(args) < len(func_params):
        for func_param in func_params:
            if func_param not in kwargs:
                kwargs[func_param] = context.get(func_param)
    return func(*args, **kwargs)


_CAMEL_CASE_PATTERN = re.compile(r"(?<!^)(?=[A-Z])")


class Context:
    def __init__(self):
        self.beans = {"name": "Ben"}
        self.definitions = {}

    def add(self, definition):
        if hasattr(definition, "__name__"):
            snake_case_name = _CAMEL_CASE_PATTERN.sub("_", definition.__name__).lower()
            self.definitions[snake_case_name] = definition

    def get(self, name):
        if name in self.beans:
            return self.beans[name]
        if name in self.definitions:
            self.beans[name] = call_in_context(self.definitions[name], self, [], {})
            return self.beans[name]
        return None


CONTEXT = Context()


def inject(func):
    CONTEXT.add(func)

    def wrapper(*args, **kwargs):
        return call_in_context(func, CONTEXT, args, kwargs)

    return wrapper
